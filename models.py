import peewee
from playhouse.shortcuts import model_to_dict

from app import db

class User(peewee.Model):
    slack_id = peewee.CharField(primary_key=True, unique=True)
    name = peewee.CharField()
    real_name = peewee.CharField()
    image_48 = peewee.CharField(null=True)
    image_72 = peewee.CharField(null=True)

    class Meta:
        db_table="User"
        database = db

    @classmethod
    def get_user(self, user_id):
        try:
            user_obj = User.get(User.slack_id == user_id)
            return model_to_dict(user_obj, exclude=['slack_id'])
        except User.DoesNotExist:
            return None

    @classmethod
    def add_user(self, user):
        user = user['user']
        try:
            with db.transaction():
                user = User.create(
                        slack_id=user['id'],
                        name=user['name'],
                        image_48=user['profile']['image_48'],
                        image_72=user['profile']['image_72']
                        )
        except peewee.IntegrityError:
            with db.transaction():
                query = User.update(
                            name=user['name'],
                            image_48=user['profile']['image_48'],
                            image_72=user['profile']['image_72']
                    ).where(User.slack_id == user['id'])
                query.execute()
