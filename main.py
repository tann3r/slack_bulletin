import sys
import time
import os

from app import app, db, sc
from event_loop import EventLoop, make_api_call

from models import *
from views import *

def create_tables():
    try:
        db.create_tables([User])
    except peewee.OperationalError:
        pass

def update_users_list():
    # users_list_dict = sc.api_call('users.list').get('members', None)
    users_list_dict = make_api_call(5, 'users.list').get('members', None)
    if users_list_dict:
        for user in users_list_dict:
            try:
                with db.transaction():
                    user = User.create(
                            slack_id=user['id'],
                            name=user['name'],
                            real_name=user['profile']['real_name_normalized'],
                            image_48=user['profile']['image_48'],
                            image_72=user['profile']['image_72']
                            )
            except peewee.IntegrityError:
                with db.transaction():
                    query = User.update(
                                name=user['name'],
                                real_name=user['profile']['real_name_normalized'],
                                image_48=user['profile']['image_48'],
                                image_72=user['profile']['image_72']
                        ).where(User.slack_id == user['id'])
                    query.execute()
    time.sleep(1)

def channel_exists():
    private_channels = [channel.get('name', None)\
        for channel in make_api_call(5, 'groups.list', exclude_archived=1).get('groups', None)]

    public_channels = [channel.get('name', None)\
        for channel in make_api_call(5, 'channels.list', exclude_archived=1).get('channels', None)]

    #if settings['channel_name'] not in (channel.get('name', None) \
            # for channel in sc.api_call("channels.list", exclude_archived=1).get('channels', None)):
    #        for channel in make_api_call(5, 'groups.list', exclude_archived=1).get('groups', None)):
    if settings['channel_name'] not in public_channels + private_channels:
                time.sleep(1)
                return False

    return True


create_tables()
if not channel_exists():
    # quickfix to prevent gunicorn running workers if channel does'nt exist
    raise Exception('No such channel, please check settings')

img_dir = os.path.join(app.root_path, 'static', 'downloaded_images')
if not os.path.exists(img_dir):
        os.makedirs(img_dir)

filelist = [f for f in os.listdir(img_dir)]
for f in filelist:
    os.remove(os.path.join(img_dir, f))

update_users_list()
event_loop = EventLoop()
event_loop.start()


if __name__ == '__main__':
    app.run()
