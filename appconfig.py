import os

class DevelopmentConfig(object):
    APP_ROOT = os.path.dirname(os.path.realpath(__file__))
    DEBUG = True
    DATABASE = os.path.join(APP_ROOT, 'slack.db')

class ProductionConfig(object):
    APP_ROOT = os.path.dirname(os.path.realpath(__file__))
    DATABASE = os.path.join(APP_ROOT, 'slack.db')
