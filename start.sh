#!/bin/bash

IFACE='enp3s0'
IP=`ifconfig $IFACE | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'`
gunicorn -w 1 --threads 2 -k gevent -b "$IP:5000" main:app


# 844880 56564 16:48
# try without gunicorn
# try define threads
# try Dozer profiler
# echo $IP
