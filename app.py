from flask import Flask
from slackclient import SlackClient
import peewee
from flask_sse import sse

from settings import settings

app = Flask(__name__)
app.config.from_object('appconfig.ProductionConfig')
app.config["REDIS_URL"] = "redis://localhost"
app.register_blueprint(sse, url_prefix='/stream')

db = peewee.SqliteDatabase(app.config['DATABASE'], threadlocals=True)
sc = SlackClient(settings['token'])
