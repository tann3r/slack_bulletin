## Slack Bulletin
###
Monitoring app for displaying tagged messages from predefined Slack channel, built with Flask.

### Installation
---
#
Clone this directory and go to the dir

`git clone git@bitbucket.org:tann3r/slack_bulletin.git && cd ./slack_bulletin`

Install requirements

`pip install -r requirements.txt`

Open `settings.py` file in any editor and fill with corresponding values, for example  
###


```python

settings = {
    'channel_name': 'my_channel_name',
    'tags': {
        'tag_one': 4, # tagname without hash sign and quantity of displayed items
        'tag_two': 3,
        'tag_three': 3,
        'tag_four': 3,
    },
    'instatag': 'tag_four',
    'comments_num': 3,
    'update_interval': 5, # seconds
    'token': ''
}
```

HTML template displays up to 4 tags (which was the client's requirement), one of them can be set as 'instatag' - messages with it will be displayed with squared images and short description. Number of comments and update interval can be set as well. Note also that for correct work of this app [Slack bot](https://api.slack.com/bot-users) (which uses [Slack RTM API](https://api.slack.com/rtm)) has to be created and set. It can be done here: https://<your-team-name>.slack.com/apps/build/custom-integration

### Run the app
---
#

Run app locally

`gunicorn -w 1 --threads 2 -k gevent -b '127.0.0.1:5000' main:app`

or expose to the local network

`gunicorn -w 1 --threads 2 -k gevent -b '<your-local-ip-here>:5000' main:app`

After that the app can be accessed via http://127.0.0.1:5000 or http://<your-local-ip-here>:5000 depending on startup method described above. 




