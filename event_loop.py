import threading
import requests
from flask_sse import sse
import time
import json
import os
import shutil
from multiprocessing.dummy import Pool as ThreadPool
from StringIO import StringIO
from Queue import Queue
from collections import OrderedDict
import gc
# import resource

from app import app, sc
from models import User
from settings import settings

num_inactive_posts = 25

def strip_tags(msg, tags):
    lines = []
    for line in msg.splitlines():
        line_without_tags = []
        for word in line.split():
            if word[1:] not in tags:
                line_without_tags.append(word)

        lines.append(' '.join(line_without_tags))

    return '\n'.join(lines)

def diff(first, second):
        second = set(second)
        return [item for item in first if item not in second]

def intersection(first, second):
    return list(set(first).intersection(second))

def get_image_link(post_dict):
    msg_id, post_dict = post_dict.popitem()
    max_attempts = post_dict['max_attempts']
    filename = post_dict['file_id'] + post_dict['filename']
    img_url_download = post_dict['img_url_download']
    img_dir = os.path.join(app.root_path, 'static', 'downloaded_images')


    for attempt in xrange(0, max_attempts):
        try:
            r = requests.get(img_url_download, headers={'Authorization': 'Bearer %s' % settings['token']}, stream=True)


            if r.status_code == 200:
                raw_img = StringIO(r.content)

                img_path = os.path.join(img_dir, filename)

                if not os.path.isfile(img_path) or raw_img.len != os.path.getsize(img_path):
                    with open(img_path, 'wb') as f:
                        raw_img.seek(0)
                        shutil.copyfileobj(raw_img, f)


                return {msg_id: os.path.join('static', 'downloaded_images', filename)}

            else:
                raise requests.ConnectionError

        except requests.ConnectionError:
            print 'Downloading %s | Connection error, retry...' % img_url_download
            if attempt == max_attempts-1:
                raise Exception('Max retries reached, exit')
            time.sleep(1)

    return None


def make_api_call(max_retries, *args, **kwargs):
    max_attempts = max_retries
    for attempt in xrange(0, max_attempts):
        try:
            result = sc.api_call(*args, **kwargs)
            return result
        except requests.ConnectionError:
            print '%s | Connection error, retry...' % args[0]
            if attempt == max_attempts-1:
                raise Exception('Max retries reached, exit')
            time.sleep(5)

class EventLoop(object):
    def __init__(self, interval=settings['update_interval']):
        self.interval = interval

        self.thread = threading.Thread(target=self.run, args=())
        self.thread.daemon = True

    def run(self):
        published_posts = {
            'active': {},
            'inactive': OrderedDict()
        }

        req_results = [channel['id'] for channel in make_api_call(30, 'channels.list', exclude_archived=1).get('channels', None)\
                        if channel['name'] == settings['channel_name']]
        channel_id = req_results[0] if req_results else None
        time.sleep(1)

        if not channel_id:
            channel_id = [channel['id'] for channel in make_api_call(30, 'groups.list', exclude_archived=1).get('groups', None)\
                            if channel['name'] == settings['channel_name']][0]
            time.sleep(1)

        while True:
            res = make_api_call(30, 'groups.history', channel=channel_id, count=200)

            posts_to_publish = {key: [] for key in settings['tags'].keys()}
            tags = ['#' + tag if tag[0] != '#' else tag for tag in settings['tags'].keys()]
            comments = {}

            for msg in res['messages']:
                msg['msg_id'] = 'msg_%s' % str(msg['ts']).replace('.', '_')
                tags_found = [tag[1:] for tag in tags if tag in msg['text'].split()]

                msg['text'] = strip_tags(msg['text'], tags_found)

                if msg.get('subtype', None) == 'file_comment':
                    if not comments.get(msg['file']['id'], None):
                        comments[msg['file']['id']] = [msg['comment']]
                    elif len(comments[msg['file']['id']]) < settings['comments_num']:
                        comments[msg['file']['id']].append(msg['comment'])

                if (msg.get('subtype', None) == 'file_share'):
                    if [key for key in msg['file'].keys() if ('thumb' in key)]:
                        msg['image'] = True

                    tags_found += [tag[1:] for tag in tags if tag in msg['file']['title'].split()]

                    if msg['file'].get('initial_comment', None):
                        if not comments.get(msg['file']['id'], None):
                            comments[msg['file']['id']] = [msg['file']['initial_comment']]
                        elif len(comments[msg['file']['id']]) < settings['comments_num']:
                            comments[msg['file']['id']].append(msg['file']['initial_comment'])

                if (msg.get('subtype', None) == 'file_share') and comments.get(msg['file']['id'], None):
                    for comment_item in comments[msg['file']['id']]:
                        tags_found += [tag[1:] for tag in tags if tag in comment_item['comment'].split()]

                tags_found = list(set(tags_found))

                for tag in tags_found:
                    if msg.get('subtype', None) in [None, 'file_share']:
                        if (msg.get('subtype', None) == 'file_share') and comments.get(msg['file']['id'], None):
                            msg['comments'] = comments[msg['file']['id']]

                            for comment in msg['comments']:
                                comment['comment'] = strip_tags(comment['comment'], tags_found)

                        user_info = User.get_user(msg['user'])
                        if not user_info:
                            # user = sc.api_call('users.info', user=msg['user'])
                            user = make_api_call(30, 'users.info', user=msg['user'])
                            User.add_user()
                        msg['user_info'] = User.get_user(msg['user'])

                        if tag == settings['instatag']:
                            posts_to_publish[tag].append(msg) if ((len(posts_to_publish[tag]) < settings['tags'][tag]) and msg.get('image', None)) else None
                        else:
                            posts_to_publish[tag].append(msg) if len(posts_to_publish[tag]) < settings['tags'][tag] else None

            # downloading images
            file_posts_to_download = {}

            for tag, posts in posts_to_publish.iteritems():
                for post in posts:
                    if post.get('image', None):
                        file_posts_to_download[post['msg_id']] = {
                            'max_attempts': 10,
                            'filename': post['file']['name'],
                            'file_id': post['file']['id'],
                            'img_url_download': post['file']['url_private_download']
                        }


            # find inactive posts in active dict, move to inactive dict
            inactive_msg_ids = diff(published_posts['active'].keys(), file_posts_to_download.keys())
            for msg_id in inactive_msg_ids:
                inactive_msg = published_posts['active'].pop(msg_id, None)
                if inactive_msg:
                    if len(published_posts['inactive']) < num_inactive_posts:
                        published_posts['inactive'][msg_id] = inactive_msg
                    else:
                        msg_id, filepath = published_posts['inactive'].popitem(0)
                        if os.path.exists(filepath):
                            os.remove(os.path.join(app.root_path, filepath))
                        published_posts['inactive'][msg_id] = inactive_msg

            # find active posts in active dict, so there will be no need to redownload
            active_msg_ids = intersection(published_posts['active'].keys(), file_posts_to_download.keys())
            for msg_id in active_msg_ids:
                del file_posts_to_download[msg_id]


            # find active posts in inactive dict, move to active dict, remove from file_posts_to download
            active_msg_ids = intersection(published_posts['inactive'].keys(), file_posts_to_download.keys())
            for msg_id in active_msg_ids:
                active_msg = published_posts['inactive'].pop(msg_id, None)
                if active_msg:
                    published_posts['active'][msg_id] = active_msg
                    del file_posts_to_download[msg_id]

            thread_pool = ThreadPool(15)
            time1 = time.time()
            pool_results = thread_pool.map_async(get_image_link, [{key: value} for key, value in file_posts_to_download.iteritems()])
            # pool_results = thread_pool.map(get_image_link, [{key: value} for key, value in file_posts_to_download.iteritems()])
            thread_pool.close()
            thread_pool.join()
            time2 = time.time()
            downloading_time = time2 - time1
            pool_results = pool_results.get()


            # add downloaded items to active posts dict
            for item in pool_results:
                if item:
                    published_posts['active'].update(item)

            # add local image url to posts
            for tag, posts in posts_to_publish.iteritems():
                for post in posts:
                    if post.get('image', None):
                        post['file']['local_url'] = published_posts['active'][post['msg_id']]


            with app.app_context():
                sse.publish({"message": json.dumps(posts_to_publish)}, type="message")

            gc.collect()

            if downloading_time < self.interval:
                time.sleep(self.interval - downloading_time)
            else:
                time.sleep(self.interval)

    def start(self):
        self.thread.start()
